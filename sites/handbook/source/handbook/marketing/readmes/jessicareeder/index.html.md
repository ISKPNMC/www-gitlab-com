---
layout: markdown_page
title: "Jessica Reeder's README"
canonical_path: "/handbook/marketing/readmes/jessicareeder/"
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

[Jessica Reeder](https://gitlab.com/jessicareeder) 
* [Sr. Campaign Manager, All-Remote](https://about.gitlab.com/job-families/marketing/all-remote-marketing/#senior-campaign-manager-all-remote) 
* [LinkedIn](https://www.linkedin.com/in/jessicareeder/)
* [Twitter](https://twitter.com/jessicareeder)
* Watch me work! You can see my (non-confidential) activity  [here](https://gitlab.com/users/jessicareeder/activity).

## About Moi
Hi! I’m Jessica. My pronouns are she/they, meaning that you can refer to me as “she/her” or as “they/them” depending on your personal preference. I’m the Senior Campaign Manager on GitLab’s All-Remote strategy & thought leadership team.

I have a very non-traditional background. I had a lot of experimental education growing up, and I have continued experimenting and innovating throughout my life. This includes my choice to **hack my own remote career** starting in about 2006, rather than commit to a traditional job track. I’ve already had an extraordinarily fulfilling career with quite a lot of time spent in the non-profit and arts sectors, ultimately leading me back to tech via ghostwriting when I was hired to ghostwrite a CEO’s memoir (which ultimately made top reading lists from Forbes to CNBC in 2021).

I discovered GitLab via a colleague’s recommendation in 2020, and was inspired by the company’s commitment to **open knowledge, experimentation (iteration), independent work, and inclusive values**. It’s been my honor to work on the All-Remote team during the COVID-19 pandemic, sharing knowledge and helping guide organizations worldwide on **how to operate in a distributed workplace.**

I’m currently working toward a master’s degree in **Organizational Psychology** at Harvard. I consider myself an expert in global remote culture, communication, and workplace design, and I’m thrilled to be exploring the greater body of knowledge behind the best practices.

My work drives me, but does not define me: I am a naturalist, hiker, punk, queer, environmentalist, writer, singer, friend, partner, weirdo, and proud auntie to 6 niblings and 6 once-removed cousins.

## What I Do
I get a lot of questions about what my job entails. It’s a unique role that intersects with Marketing, People, and Operations. I also take on ad-hoc and cross-functional projects to support and contribute to organizational culture and values-based outcomes.

Here’s a list of some of the results and outcomes I’ve driven since spring 2020.

### GitLab Initiatives & Projects

#### FY23
1. Member, [DIB Advisory Board](https://about.gitlab.com/company/culture/inclusion/advisory-group-members/) 
1. Founding member, Sustainability & Social Impact Advisory Board (WIP)
1. Mentor, [Women at GitLab mentorship program](https://about.gitlab.com/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/) 
1. DRI: [All-Remote landing page redesign](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/issues/22) 

#### FY22
1. DRI: [Livestreaming GitLab’s IPO](https://about.gitlab.com/blog/2021/10/14/gitlab-inc-takes-the-devops-platform-public/)
	* I led the team that produced the first-ever end-to-end livestream of a listing day on NASDAQ, with 19,000 live viewers and 5 hours of content.
	* I also led the production of the internal-facing team event, communications, and day-of activities.
1. DRI:  [Async Weeks](https://about.gitlab.com/handbook/marketing/corporate-marketing/all-remote/#async-weeks)  initiative
	* Founded this deep-work initiative that was adopted throughout GitLab and external organizations.
1. DRI:  [REMOTE by GitLab](https://remotebygitlab.com/) 
	* Nearly 4,000 registrations for our inaugural half-day event on the future of work, with speakers from Twitter, Zoom, Facebook, Harvard Business School, INSEAD, and many more.
1. DRI:  [Remote Work Report](https://about.gitlab.com/remote-work-report/) 
	* With almost 4,000 respondents on six continents, this is one of the most in-depth independent studies on the future of distributed work.
1. DRI:  [Out of the Office Report](https://about.gitlab.com/out-of-the-office/) 
	* This qualitative survey predicted the Great Resignation in early 2021.
1. DRI:  [GitLab Assembly](https://about.gitlab.com/company/gitlab-assembly/)  (Q1 FY22)
	* Produced the first interactive all-hands event for the fiscal year kickoff. 
1. DRI:  [Team building consultation](https://gitlab.com/gitlab-com/people-group/people-business-partners/-/issues/19)  (confidential issue) for CRO org
	* Worked with the People Business Partner for the CRO function on strategies to increase engagement.
1. DRI:  [Content & SEO improvements](https://gitlab.com/groups/gitlab-com/marketing/-/epics/2414) , all-remote guides
	* Led this initiative to increase usability and effectiveness of our all-remote library of content.
1. DRI: [Year-end donation drive](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/5529)
	* This internal engagement initiative drove charitable donations of $720,000+ and over 770 volunteer hours, all as individual gifts from GitLab team members.
1. Contrib: [The Remote Playbook](https://learn.gitlab.com/allremote/remote-playbook)
1. Contrib: Transgender Day of Visibility (confidential initiative)

#### FY21
1. DRI: Coursera:  [How to Manage a Remote Team](https://www.coursera.org/learn/remote-team-management) 
	* Led initiative in partnership with Coursera to create a 10-hour course on remote operations, transition, and management best practices. Currently at 30,000+ enrollments and 4.9 star rating.
	* Press coverage: [Fortune](https://fortune.com/2020/09/22/work-from-home-class-coursera-gitlab/) / [Yahoo](https://www.yahoo.com/now/coursera-business-launches-leadership-academy-130000275.html)  
1. DRI: Universal Remote webcast ([sample video](https://www.youtube.com/watch?v=9xvVMglCm6I))
	* In the early days of the COVID-19 pandemic, this webcast sought to elevate remote work experts and inform teams who were new to distributed work.
1. DRI: All-Remote newsletter (deprecated)
	* In the early days of the COVID-19 pandemic, GitLab’s remote-work newsletter shipped regular updates with the latest updates, news, and best practices to over 30,000 subscribers.
1. DRI:  [Meal donation drive](https://about.gitlab.com/handbook/people-group/givelab-volunteer-initiatives/#gitlab-donation-drives)  
	* An internal engagement initiative, this charitable giving program funded 100,000 meals at food banks in 19 countries through team member donations.


### Speaking & Media

#### FY22
1. Panelist: The Culture Summit -  [“Culture Deep Dive With GitLab”](https://www.culturesummit.co/home/speakers/)   
1. Host/MC: REMOTE by GitLab - Elevating Culture Track ( [video](https://www.youtube.com/watch?v=vnZIq6CzT0E) )
1. Interview: Atlassian Work Check Podcast -  [Should you get your team in sync, async?](https://www.atlassian.com/blog/podcast/work-check/season/season-1/should-you-get-your-team-in-sync-async) 
1. Interview: Citrix Remote Works Podcast - [Remote Collaboration that Breeds Innovation](https://ceo.digital/meet-the-innovators/citrix/remote-collaboration-innovation) 
1. Interview: Axios - [The case for going back to the office](https://www.axios.com/should-you-go-back-to-office-f03e7ce4-5491-4520-8422-ac52310f239f.html) 
1. Interview: Tech Monitor - [‘All-Remote’ working: How GitLab’s 1,300 staff collaborate from home](https://techmonitor.ai/leadership/remote-working-according-to-gitlab) 
1. Interview: Gather - [Empowering First-time Managers](https://www.teamgather.co/blog/empowering-first-time-managers) 
1. Interview: Workplaceless - [20 Habits of Healthy and Productive Remote Teams](https://www.workplaceless.com/blog/habits-of-healthy-and-productive-remote-teams) 
1. Interview: WIRED - [What Will Work Look Like in 2022? (Hint: Not the Metaverse)](https://www.wired.com/story/the-future-of-work-2022/) 
1. Panelist: Commsor Community Summit - [Internal Community & Remote Work](https://www.commsor.com/post/community-led-summit-learnings) 
1. Panelist: 9punto5 - [The “future of work” after a pandemic](https://www.9punto5.cl/) 

#### FY21
1. Host/interviewer: Universal Remote webcast - [Rethinking Remote Management](https://www.youtube.com/watch?v=dsnSb-lpZSI) 
1. Speaker: ScaleUp 360 - [GitLab and Remote Work - a holistic approach to leading enterprise transformation](https://www.scale-up-360.com/en/software-delivery/agenda#/) 
1. Interview: Fast Company -  [Extremely Transparent and Incredibly Remote: GitLab’s radical vision for the future of work](https://www.fastcompany.com/90548691/extremely-transparent-and-incredibly-remote-gitlabs-radical-vision-for-the-future-of-work) 
1. Interview: FranceInfo - États-Unis:  [le télétravail ne séduit pas tout le monde](https://www.francetvinfo.fr/monde/usa/etats-unis-le-teletravail-ne-seduit-pas-tout-le-monde_4126711.html) 

### Consultations With External Organizations

1. Google
	* Met with the internal community team in the Product/Eng organization to talk through best practices and approaches for a return to hybrid work.
1. Porsche Digital
	* Worked with the remote transformation team as they began to develop the future of their workplace. They later went on to [create significant innovations](https://medium.com/next-level-german-engineering/creating-the-new-normal-how-porsche-digitals-approach-to-work-changed-7c3490cb49f3) in Porsche Digital's workplace design.
1. ING
	* Joined ING's internal team for discussions on remote teambuilding, communication, and collaboration (as well as meeting strategy).
1. LEGO
	* Provided best practices and strategies for collaboration, creativity, project management, and documentation as a distributed team.
1. Vale
	* Consulted on scaling remote best practices for a fast-growing technical team across multiple countries.
1. Lufthansa Cargo
	* Met with executives to discuss remote transformation approaches, culture, and management. You can [read about Lufthansa's outcomes here](https://businesschief.eu/interviews/lufthansa-cargo-covid-19-catalyst-digital-switch-1).
7. Cribl
	* Ongoing consultations on scaling remote operations and culture, with a focus on HR, onboarding, and logistics.

## Working With Me
* I strive to be open, welcoming, respectful, a good listener, a great team player, and fun to be with. I am always open to a coffee chat, a brainstorm (love!!), new idea, or problem-solving session.
* When I’m not in those types of calls, I love to go into deep focus mode. I often walk to think, and am frequently known to go completely heads down on a project until I emerge with a full strategy and plan.
* As a mentor and manager, I tend to default to solutioning when I should do more listening. Feel free to call me out on that.
* As a report, I thrive with minimal management and plenty of opportunities to work creatively and innovate. However, I do appreciate having a manager who listens, connects, helps me solve problems, and makes it clear they have my best interests in mind. My  [love languages](https://www.5lovelanguages.com/learn)  are Acts of Service and Quality Time.
* I had a psychologically challenging but very creative childhood, and spent several of my adult years in creatively-fulfilling, surrounded-by-love, deep poverty. This made me much wiser and more self-reliant, and it also made me an iconoclast with a highly sensitive bullshit detector. If you’re ever wondering why I’m not responding the way you expect to a conversation or meeting, I’m probably just receiving the information in my own special way! Give me time.
* I don’t fully subscribe to Meyers-Briggs personality types, but as a convenient shorthand, you can think of me as [INTP](https://www.16personalities.com/intp-personality). I am reserved, logical, and process-oriented. I often prefer to think before speaking and can move much slower than most people. Tortoise, not hare.
* I am deeply committed to inclusion & belonging as supportive strategies to increase global, racial, gender, ability, and other dimensions of diversity and representation. Sometimes I may come across as trying too hard - please call me out if I’m being weird about it. I’m working to strengthen my understanding of standpoint epistemology, inspired by [ this essay](https://www.thephilosopher1923.org/essay-taiwo) .
* I’ve been an environmentalist since I was born. Remote work is just part of my lifelong effort to minimize my personal impact and support more sustainable behaviors on a global scale.




Return to the main [Marketing handbook section](/handbook/marketing/).

